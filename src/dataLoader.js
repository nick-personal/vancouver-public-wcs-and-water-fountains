import axios from 'axios';
import Collection from './models/Collection';

export default (key) => {
	return new Promise((resolve, reject) => {
		let data = localStorage.getItem(key);
		if (data !== null) {
			data = JSON.parse(data);
			resolve(new Collection(data, key));
		}
		else {
			axios.get('https://opendata.vancouver.ca/api/records/1.0/search', {
				params: {
					dataset: key,
					rows: 1000,
				},
			})
			.then((resp) => {
				data = resp.data.records;
				localStorage.setItem(key, JSON.stringify(data));
				resolve(new Collection(data, key));
			})
			.catch((e) => {
				reject(e);
			});
		}
	});
};
