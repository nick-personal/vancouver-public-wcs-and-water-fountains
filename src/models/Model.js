import tt from '@tomtom-international/web-sdk-maps';
import FountainIcon from '../images/d-icon.png';
import WCIcon from '../images/wc-icon.png';

export default class Model {

	constructor(data) {
		if (!data || typeof data !== 'object') {
			throw new Error('Wrong paramenters for constructor.');
		}

		for (let prop in data) {
			this[prop] = data[prop];
		}
	}

	get lat() {
		return this.geom.coordinates[1] || null;
	}

	get lng() {
		return this.geom.coordinates[0] || null;
	}

	addMarker(map) {
		const isWashroom = (this.type === 'public-washrooms');

		const markerElement = document.createElement('div');
		markerElement.className = 'marker ' + this.type;
		markerElement.title = isWashroom ? 'Washroom' : 'Drinking fountain';

		const markerContentElement = document.createElement('div');
		markerContentElement.className = 'marker-content ' + this.type;
		
		markerElement.appendChild(markerContentElement);
		
		const iconElement = new Image();
		iconElement.className = 'marker-icon';
		iconElement.src = isWashroom ? WCIcon : FountainIcon;
		
		markerContentElement.appendChild(iconElement);

		let tableHTMLContent = `
			<tr>
				<td><strong>Area</strong></td>
				<td>${this.geo_local_area || 'No data'}</td>
			</tr>
			<tr>
				<td><strong>Location</strong></td>
				<td>${this.location || 'No data'}</td>
			</tr>
			<tr>
				<td><strong>Maintainer</strong></td>
				<td>${this.maintainer || 'No data'}</td>
			</tr>
		`;
		
		if (isWashroom) {
			tableHTMLContent += `
				<tr>
					<td><strong>Address</strong></td>
					<td>${this.address || 'No data'}</td>
				</tr>
				<tr>
					<td><strong>Summer hours</strong></td>
					<td>${this.summer_hours || 'No data'}</td>
				</tr>
				<tr>
					<td><strong>Winter hours</strong></td>
					<td>${this.winter_hours || 'No data'}</td>
				</tr>
				<tr>
					<td><strong>Wheelchair</strong></td>
					<td>${this.wheel_access || 'No data'}</td>
				</tr>
				<tr>
					<td><strong>Note</strong></td>
					<td>${this.note || 'No data'}</td>
				</tr>
			`;
		}
		else {
			tableHTMLContent += `
				<tr>
					<td><strong>In operation</strong></td>
					<td>${this.in_operation || 'No data'}</td>
				</tr>
			`;
		}

		const popup = new tt.Popup({offset: 30, className: 'popup-box ' + this.type}).setHTML(`
			<h2>${this.name || 'No name'}</h2>
			<table>${tableHTMLContent}</table>
		`);
		
		new tt.Marker({element: markerElement, anchor: 'bottom'})
			.setLngLat([this.lng, this.lat])
			.setPopup(popup)
			.addTo(map);
	}

}
