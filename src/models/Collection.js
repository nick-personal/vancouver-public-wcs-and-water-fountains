import Model from './Model';

export default class Collection {
	
	constructor(data = [], key) {
		if (!data || !Array.isArray(data)) {
			throw new Error('Wrong paramenters for constructor.');
		}

		this.type = key;
		this.models = data.map(el => {
			const model = new Model(el.fields)
			model.type = key;
			return model;
		});
		
	}

	addMarkers(map) {
		this.models.forEach(model => model.addMarker(map));
	}

}