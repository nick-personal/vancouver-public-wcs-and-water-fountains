import '@tomtom-international/web-sdk-maps/dist/maps.css';
import './scss/styles.scss';
import './mobile-or-tablet';

import tt from '@tomtom-international/web-sdk-maps';
import dataLoader from './dataLoader';

Promise.all([
	dataLoader('public-washrooms'), 
	dataLoader('drinking-fountains'),
]).then((values) => {
	const [washrooms, fountains] = values;
	washrooms.addMarkers(map);
	fountains.addMarkers(map);
}).catch((e) => {
	// console.log(e);
});

const map = tt.map({
	key: process.env.TOMTOM_API_KEY,
	container: 'map',
	style: 'tomtom://vector/1/basic-main',
	center: { lat: 49.25, lng: -123.1 },
	zoom: 12,
	dragPan: !isMobileOrTablet(),
});

map.addControl(new tt.FullscreenControl());
map.addControl(new tt.NavigationControl());
